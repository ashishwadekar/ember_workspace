import Ember from 'ember';

export default Ember.Controller.extend({
  actions: {
    toggleCaStatus(company){
      company.toggleProperty('ca_status');
      company.save();
    }
  }
});

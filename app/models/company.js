import DS from 'ember-data';

export default DS.Model.extend({
  name: DS.attr('string'),
  activation_date: DS.attr('date'),
  phone: DS.attr('string'),
  ca_status: DS.attr('boolean')
});
